<?php
	include "/var/www/html/webservices/serviceInfo.php";
	if(verifyToken()) {
        header('Location: /home.php');
	}
	###BEGIN GENERATION###
	echo "
    <!DOCTYPE html>
<html lang='en'>
	<head>
        <meta charset='utf-8'>
        <meta http-equiv='X-UA-Compatible' content='IE=edge'>
        <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'>
        <meta name='description' content='Login for 780Webserver services'>
        <meta name='author' content='Giakhanh hoang'>
		<title>780Webserver</title>
        <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css' integrity='sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M' crossorigin='anonymous'>
	</head>
    <body>
        <div class='container' style='padding-top:10%; font-family:arial'>
            <div class='row justify-content-center'>
                <div class='col-lg-5'>
                    <div class='row justify-content-end'>
                        <div class='col-lg-12 text-right'>
                            <img src='webimgs/cube2_clean_large.png' alt='Dev Logo' style='max-width:120px; display:inline-block'>
                        </div>
                    </div>
                    <div class='row justify-content-end align-items-end'>
                        <div class='col-lg-12'>
                            <p class='text-right' style='font-size:48px'>
                                780Webservices
                            </p>
                        </div>
                    </div>
                </div>
                <div class='col-lg-4' style='border-style:solid; border-width: 0px 0px 0px 1px; border-color:#E5E5E5'>
                    <div class='row'>
                        <div class='col-lg-12'>
                            <h3>Login</h3>
                            <hr>
                            <form>
                                <input class='form-control' type='text' name='uname' placeholder='Username' style='width:300px'>
                                <input class='form-control' type='password' name='pw' placeholder='Password' style='width:300px; margin-top:3px'>
                                <p></p>
                                <button class='btn btn-primary' type='submit' name='serreqt' value='verifyLogin' formmethod='POST' formaction='webservices/loading.php'>
                                    Login
                                </button>
                                <button class='btn btn-primary' type='submit' formmethod='POST' formaction='webservices/accountCreate.html'>
                                    Create Account
                                </button>
				            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class='row justify-content-center'>
                <div class='col-lg-10' style='padding-top:20px'>
                    <hr>
                    <p class='text-center' style='color:slategray'>
                        780Development - Some services remain under development - Not a 24/7 Service
                    </p>
                </div>
            </div>
        </div>
    </body>
</html>
    ";
?>