/**
	780Development
	Giakhanh Hoang
	
	/webservices/webservices.js
	UI and account handling scripts
	
 */

// Page to redirect to after login or token authentication
var loginRedirect = 'home.html';

window.onload = function () {
    // Set login redirect if redirect cookie is set
    // WARN: Not secure, someone could set this cookie and redirect themselves
    var savedLoginRedirect = getCookie('loginRedirect');
    if(savedLoginRedirect != null && savedLoginRedirect != '') {
        loginRedirect = savedLoginRedirect;
        // And delete the cookie
        document.cookie = 'loginRedirect=';
    }
    
    // Resize elements according to the browser screen size
    checkElementsForResize();
    
    // Authenticate token first so that if the user has already logged in, they can skip
    //   immediately to wherever they need to go
    //authenticateToken();
    validateToken();
}

window.onresize = function () {
    // Resize elements as the browser screen size changes
    checkElementsForResize();  
}

// Some anonomyous function required for the modal to be actived?
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

/*
    Resizes elements based on the browser screen size.
    Applies only to:
        - index.html
        - webservices/createAccount.html
 */
function checkElementsForResize() {
    if(window.innerWidth <= 480 && window.innerWidth > 360) {
        document.getElementById('title-text').style.fontSize = '42px';
    } else if (window.innerWidth <= 360) {
        document.getElementById('title-text').style.fontSize = '36px';
    } else {
        document.getElementById('title-text').style.fontSize = '48px';
    }
    if(window.innerWidth < 992) {
        document.getElementById('title-icon').className = 'col-lg-12';
        document.getElementById('title-text').className = '';
    } else {
        document.getElementById('title-icon').className = 'col-lg-12 text-right';
        document.getElementById('title-text').className = 'text-right';
    }
}

/*
    Gets the current values of each input field in the login page, perform preliminary validation,
      and sends it to the server.
 */
function sendLoginRequest() {
    var username = document.getElementById('uname').value;
    var pass = document.getElementById('pw').value;
    var stayLog = (document.getElementById('staylog').checked ? stayLog = "true" : stayLog = "false");
    
    if(!sanitizeSQLInput(username) || !sanitizeSQLInput(pass))
        document.getElementById('sys-feedback').innerHTML = 'Invalid credentials';
    else validateLogin(username, pass, stayLog);
}

function sendCreateAccountRequest() {
    var email = document.getElementById('email').value;
    var user = document.getElementById('user').value;
    var pass = document.getElementById('pass').value;
    var repass = document.getElementById('repass').value;
    var pkey = document.getElementById('pkey').value;
    if(!validateEmailFormat(email))
        document.getElementById('sys-feedback').innerHTML = 'Bad email format';
    else if(!sanitizeSQLInput(user))
        document.getElementById('sys-feedback').innerHTML = 'Username can only contain 0-9, A-Z, a-z, and _';
    else if(pass != repass)
        document.getElementById('sys-feedback').innerHTML = 'Passwords do not match';
    else if(!sanitizeSQLInput(pass))
        document.getElementById('sys-feedback').innerHTML = 'Password can only contain 0-9, A-Z, a-z, and _';
    else if(!sanitizeSQLInput(pkey)) 
        document.getElementById('sys-feedback').innerHTML = 'Invalid key';
    else createAccount(email, user, pass, pkey);
}

function sendPasswordResetRequest() {
    var username = document.getElementById('uname').value;
    var pass = document.getElementById('pw').value;
    var ckey = document.getElementById('ckey').value;
    if(!sanitizeSQLInput(username) || !sanitizeSQLInput(pass))
        document.getElementById('sys-feedback').innerHTML = 'Invalid input';
    else resetPassword(username, pass, ckey);
}

/*
    Sets the cookie acknowledging the user has read the server disclaimer notice/warning.
 */
function viewedNotice() {
    document.cookie = 'viewedServerNotice=true';
}


function onAuthOpPass(pkg, responseText) {
    console.log("Authentication operation successful");
    console.log(responseText);
    document.location = loginRedirect;
}

function onAuthOpFail(pkg, responseText) {
    console.log("Authentication operation failed");
    console.log(responseText);
    
}
/**
    /wslib/auth/auth.js handling functions below
 */
function onAuthPass(responseText) {
    document.location = loginRedirect;
}

function onAuthFail(responseText) {
    document.getElementById('loading-div').style.display = 'none';
    if(getCookie('viewedServerNotice') != 'true') {
        $("#serverWarningModal").modal("show")
    }
}

function onLoginPass(responseText) {
    document.location = loginRedirect;
}

function onLoginFail(responseText) {
    document.getElementById('sys-feedback').innerHTML = parseAuthError(responseText);
}

function onCreateAccPass(responseText) {
    document.location = '/';
}

function onCreateAccFail(responseText) {
    document.getElementById('sys-feedback').innerHTML = parseAuthError(responseText);
}