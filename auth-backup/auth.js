/**
	780Development
	Giakhanh Hoang
	
	/wslib/auth/auth.js
	Client authentication script
	
 */
 
function authenticateToken() {
	var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            var responseText = this.responseText;
            console.log(responseText);
            if(responseText.includes('//AUTH_SUCCESS')) {
				onAuthPass(responseText);
            } else {
				onAuthFail(responseText);
            }
        }
    }
    xmlhttp.open('POST', '/wslib/auth/auth.php', true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send('serreqt=verifyToken');
}

function authenticateLogin(username, pass, staylog) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            var responseText = this.responseText;
            console.log(responseText);
            if(responseText.includes('//LOGIN_SUCCESS')) {
				onLoginPass(responseText);
            } else {
				onLoginFail(responseText);
            }
        }
    }
    xmlhttp.open('POST', '/wslib/auth/auth.php', true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send('serreqt=verifyLogin&uname=' + username + '&pw=' + pass + '&staylog=' + staylog);
}

function createAccount(email, user, pass, pkey) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            var responseText = this.responseText;
            console.log(responseText);
            if(responseText.includes('//AUTH_ACCOUNT_CREATED')) {
				onCreateAccPass(responseText);
            } else {
				onCreateAccFail(responseText);
            }
        }
    }
    xmlhttp.open('POST', '/wslib/auth/auth.php', true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send('serreqt=createAccount&email=' + email + '&uname=' + user + '&pw=' + pass + '&pkey=' + pkey);
}

function resetPassword(user, pass, ckey) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            var responseText = this.responseText;
            console.log(responseText);
            if(responseText.includes('//AUTH_ACCOUNT_CREATED')) {
				onCreateAccPass(responseText);
            } else {
				onCreateAccFail(responseText);
            }
        }
    }
    xmlhttp.open('POST', '/wslib/auth/auth.php', true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send('serreqt=resetPassword&uname=' + user + '&pw=' + pass + '&ckey=' + ckey);
}

function logout() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            var responseText = this.responseText;
            document.location = '/';
        }
    }
    xmlhttp.open('POST', '/wslib/auth/auth.php', true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send('serreqt=logout');
}

function parseAuthError(response) {
    var code = '';
    if(response.includes('//ERROR')) {
        code = response.split('//')[1];
    }
    switch(code) {
        case 'ERROR-100': return 'Invalid credentials';
        case 'ERROR-101': return 'Invalid token';
        case 'ERROR-103': return 'Unknown Error - 103';
        case 'ERROR-104': return 'Invalid credentials';
        case 'ERROR-112': return 'Invalid key';
        case 'ERROR-119': return 'Failed to create account';
        case 'ERROR-199': return 'Invalid credentials';
        case 'ERROR-300': return 'Unknown Error - 300';
        case 'ERROR-999': return 'Unknown Error - 999';
        default: return response;
    }
}