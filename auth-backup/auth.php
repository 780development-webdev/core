  <?php
	/**
		780Development
		Giakhanh Hoang
		
		/wslib/auth/auth.php
		
		This is the main authentication file that should be used to submit authentication requests
		  to.
	 */
	 
	# Library of crucial functions, specifically relating to authentication
    include "/var/www/html/wslib/auth/authlib.php";
	
    //////////////////////////////////////////////////////////////////////////
	///// START OF PROGRAM ///// START OF PROGRAM ///// START OF PROGRAM /////
	
	# Service request processing
	$SERREQT = $_POST['serreqt'];
	
	# Handle service request, echo the final status state
	if(determineRequest($SERREQT)) {
		echo "//OP_SUCCESS";
	} else {
		echo "//OP_FAILURE";
	}
	
	///// END OF PROGRAM ///// END OF PROGRAM ///// END OF PROGRAM  /////
	/////////////////////////////////////////////////////////////////////
	
	/**
		Discerns the request that needs to be handled and returns the success status of the
		  operation.
	 */
	function determineRequest($requestType) {
	    if ($requestType == 'verifyLogin') {
			return verifyLogin();
	    } elseif ($requestType == 'createAccount') {
			return createAccount();
	    } elseif ($requestType == 'verifyToken') {
			return verifyToken();
	    } elseif ($requestType == 'passwordChange') {
			return false;
		} elseif ($requestType == 'resetPassword') {
			return resetPassword();
		} elseif ($requestType == 'emailChange') {
			return false;
		} elseif ($requestType == 'sessionClear') {
			return false;
		} elseif ($requestType == 'publicKey') {
			return getPublicKey();
		} elseif ($requestType == 'logout') {
			return logout();
		} else {
            return false;
        }
	}
?>