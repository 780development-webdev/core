<?php
	/**
		780Development
		Giakhanh Hoang
		
		/wslib/auth/authlib.php
		
		This file contains core functions related to authentication.
		This includes so far:
		- Password hasing and encryption
        - Password reset
		- Account creation
		- Login and token verification
        - Logout operations
		
		Error messages:
		100 - Invalid user
		101 - Failed to validate user token
		103 - Failed to set cookie
		104 - Invalid credentials
		**111 - Password mismatch/invalid - DEPRECIATED
		112 - Failed to validate key
		119 - Failed to create account
		199 - Login operations failed
		200 - Unsuccessful password change
		201 - Key certification failed
		202 - Unsuccessful key deletion
		300 - Database operations failed
		999 - Unknown error
	 */
	 
	// Important credentials file for accessing the MySQL Database
	// This is not accessible to anyone including developers expect the owner
    include "/var/wkeys/skey.php";
	
	include "/var/www/html/wslib/auth/enc.php";
	include "/var/www/html/wslib/svc/services.php";
	include "/var/www/html/wslib/util/util.php";
	
	if(!function_exists('verifyToken')) {
		function verifyToken() {
			// Ensure a token exists
			if(!isset($_COOKIE['web_userToken'])) {
                echo "//ERROR-101";
				return false;
			} else {
				$CURUSER = $_COOKIE['web_username'];
			}
			// Login to database
			if(!mysqli_connect(HOST, USER, PASS, 'web_service')) {
				// Database connection failed
				echo "//ERROR-300";
				return false;
			} else {
				$con = mysqli_connect(HOST, USER, PASS, 'web_service');
			}
			// Select the token column from the row with the logged in user
			// TODO: Should select from token list and search all sessions for the valid one
			$tokenRes = mysqli_query_execute_and_result($con, 
				"SELECT token FROM accounts WHERE username= ?",
				"s",
				$CURUSER
			);
			if($tokenRes != "") {
				// Validate token
				// Uncomment when ready to implement
				if(sodium_crypto_pwhash_str_verify($tokenRes, $_COOKIE['web_userToken'])) {
					echo "//AUTH_SUCCESS";
					mysqli_close($con);
					return true;
				} else {
					// Invalid or expired user token
					echo "//ERROR-101";
				}
			} else {
				// Invalid user
				echo "//ERROR-100";
			}
			mysqli_close($con);
			return false;
		}
	}
    
	if(!function_exists('verifyLogin')) {
		function verifyLogin() {
			$CURUSER = $_POST['uname'];
			$CURPASS = $_POST['pw'];
			$STAYLOG = $_POST['staylog'];
			
			if(!mysqli_connect(HOST, USER, PASS, 'web_service')) {
				// Database connection failed
				echo "//ERROR-300";
				return false;
			} else {
				$con = mysqli_connect(HOST, USER, PASS, 'web_service');
			}
			$userRes = mysqli_query_execute_and_result($con,
					"SELECT username FROM accounts WHERE username = ?",
					"s",
					$CURUSER
			);
			if($userRes != "") {
				$passRes = mysqli_query_execute_and_result($con,
					"SELECT password FROM accounts WHERE username = ?",
					"s",
					$CURUSER
				);
				// Uncomment when ready to implement
				if(sodium_crypto_pwhash_str_verify($passRes, $CURPASS)) {
					if($STAYLOG) {
						$DUR = time() + 2592000; #Cookie will last for a month
					} else {
						$DUR = 0;
					}
					
					// Should simplify operations...
					$ranString = generateRandomString();
					$hash = hash_password($ranString);
					setcookie('web_userToken', $ranString, $DUR, "/");
					setcookie('web_username', $CURUSER, $DUR, "/");
					
					$cmd = mysqli_prepare($con, "UPDATE accounts SET token = ? WHERE username = ?");
					mysqli_stmt_bind_param($cmd, "ss", $hash, $CURUSER);
					if(mysqli_stmt_execute($cmd)) {
						// Successful login
						echo "//LOGIN_SUCCESS";
						mysqli_stmt_close($cmd);
						mysqli_close($con);
						return true;
					} else {
						// Login failed to set cookie
						mysqli_stmt_close($cmd);
						echo "//ERROR-103";
					}
				} else {
					// Failed to validate credentials
					echo "//ERROR-104";
				}
			} else {
				// Login operations failed
				echo "//ERROR-199";
			}
			mysqli_close($con);
			return false;
		}
	}
    
	if(!function_exists('createAccount')) {
		function createAccount() {
			$EMAIL = $_POST['email'];
			$NEWUSER = $_POST['uname'];
			$NEWPASS = $_POST['pw'];
			$VERIFYKEY = $_POST['pkey'];
			
			if(!mysqli_connect(HOST, USER, PASS, 'web_service')) {
				echo "//ERROR-300";
				return false;
			} else {
				$con = mysqli_connect(HOST, USER, PASS, 'web_service');
			}
			
			$hash = hash_password($NEWPASS);
			
			// Verify Permission Key
			$pkeyRes = mysqli_query_execute_and_result($con, 
				"SELECT permission_key FROM permission_keylist WHERE permission_key = ?",
				"s",
				$VERIFYKEY
			);
			if($pkeyRes == $VERIFYKEY) {
				// Create account
				$cmd = mysqli_prepare($con, "INSERT INTO accounts(username, password, email, token) VALUES (?, ?, ?, 'New User')");
				mysqli_stmt_bind_param($cmd, "ssss", $NEWUSER, $hash, $EMAIL);
				if(mysqli_stmt_execute($cmd)) {
					mysqli_stmt_close($cmd);
					// Delete permission key
					// TODO: Verify deletion of permission key
					mysqli_query_and_execute($con, 
						"DELETE FROM permission_keylist WHERE permission_key = ?",
						"s",
						$VERIFYKEY
					);
					echo "//AUTH_ACCOUNT_CREATED";
					mysqli_close($con);
					return true;
				} else {
					// Failed to create account
					mysqli_stmt_close($cmd);
					echo "//ERROR-119";
				}
			} else {
				// Failed to validate key
				echo "//ERROR-112";
			}
			mysqli_close($con);
			return false;
		}
	}
	
	if(!function_exists('changePassword')) {
		function changePassword() {
			$CURUSER = $_POST['uname'];
			$CURPASS = $_POST['opw'];
			$NEWPASS = $_POST['pw'];
			
			// Hash new password
			$hash = hash_password($NEWPASS);
			
			if(!mysqli_connect(HOST, USER, PASS, 'web_service')) {
				echo "//ERROR-300";
				return false;
			} else {
				$con = mysqli_connect(HOST, USER, PASS, 'web_service');
			}
			
			// Validate user existance
			$userRes = mysqli_query_execute_and_result($con,
					"SELECT username FROM accounts WHERE username = ?",
					"s",
					$CURUSER
			);
			if($userRes != "") {
				$passRes = mysqli_query_execute_and_result($con,
					"SELECT password FROM accounts WHERE username = ?",
					"s",
					$CURUSER
				);
				if(sodium_crypto_pwhash_str_verify($passRes, $CURPASS)) {
					$cmd = mysqli_prepare($con, "UPDATE accounts SET password= ? WHERE username = ?");
					mysqli_stmt_bind_param($cmd, "ss", $hash, $CURUSER);
					if(mysqli_stmt_execute($cmd)) {
						// Successful password change
						echo "//PW_CHANGE_SUCCESS";
						mysqli_stmt_close($cmd);
						mysqli_close($con);
						return true;
					} else {
						// Unsuccessful password change
						mysqli_stmt_close($cmd);
						echo "//ERROR-200";
					}
				} else {
					// Failed to validate credentials
					echo "//ERROR-104";
				}
			} else {
				// Login operations failed
				echo "//ERROR-199";
			}
			mysqli_close($con);
			return false;
		}
	}
	
	/**
		Password Reset - Using a certification key for user verification
	 */ 
	if(!function_exists('resetPassword')) {
		function resetPassword() {
			$CURUSER = $_POST['uname'];
			$NEWPASS = $_POST['pw'];
			$CKEY = $_POST['ckey'];
			
			// Hash new password
			$hash = hash_password($NEWPASS);
			
			if(!mysqli_connect(HOST, USER, PASS, 'web_service')) {
				echo "//ERROR-300";
				return false;
			} else {
				$con = mysqli_connect(HOST, USER, PASS, 'web_service');
			}
			
			// Validate certification key
			$ckeyRes = mysqli_query_execute_and_result($con, 
				"SELECT certification_key FROM certification_keylist WHERE certification_key = ?",
				"s",
				$CKEY
			);
			if($ckeyRes == $CKEY) {
				// Validate user existance
				$userRes = mysqli_query_execute_and_result($con,
					"SELECT username FROM accounts WHERE username = ?",
					"s",
					$CURUSER
				);
				if($userRes != "") {
					// Update password
					$cmd = mysqli_prepare($con, "UPDATE accounts SET password= ? WHERE username = ?");
					mysqli_stmt_bind_param($cmd, "ss", $hash, $CURUSER);
					if(mysqli_stmt_execute($cmd)) {
						mysqli_stmt_close($cmd);
						// Remove certification key as it is used
						$ckeyRes = mysqli_query_and_execute($con, 
							"DELETE FROM certification_keylist WHERE certification_key = ?",
							"s",
							$CKEY
						);
						if($ckeyRes) {
							// Successful password change
							echo "//PW_RESET_SUCCESS";
							mysqli_close($con);
							return true;
						} else {
							// Unsuccessful key deletion
							echo "//ERROR-202";
						}
					} else {
						// Unsuccessful password change
						mysqli_stmt_close($cmd);
						echo "//ERROR-200";
					}
				} else {
					// Login operations failed
					echo "//ERROR-199";
				}
			} else {
				// Key certification failed
				echo "//ERROR-201";
			}
			mysqli_close($con);
			return false;
		}
	}
	
    /**
        Logout Operations
     */
    if(!function_exists('logout')) {
		function logout() {
            // Set username for database query
            if(isset($_COOKIE['web_userToken'])) {
				$CURUSER = $_COOKIE['web_username'];
                // Login to database to remove token
                if(!mysqli_connect(HOST, USER, PASS, 'web_service')) {
                    // Database connection failed
                    echo "//ERROR-300";
                    return false;
                } else {
                    $con = mysqli_connect(HOST, USER, PASS, 'web_service');
                }
                // Delete token attribute value from the user's tuple
                mysqli_query_and_execute($con, 
                    "UPDATE accounts SET token='' WHERE username= ?",
                    "s",
                    $CURUSER
                );
                mysqli_close($con);
			}

            // Delete other web service cookies
            clearWebServicesCookies();

            // Delete token
			deleteCookie('web_userToken', '/');

            // Delete remaining web services cookies
			deleteCookie('web_username', '/');
            
            echo "//LOGOUT-SUCCESS";
			return true;
		}
	}
?>