<?php
	include "/var/www/html/wslib/util/util.php";

    if(!function_exists("clearWebServicesCookies")) {
		function clearWebServicesCookies() {
            // Service Paths
			$SVC_HOME_DASHBOARD_PATH = "/home-dashboard";
            $SVC_CALENDAR_PATH = "/app/calendar";

			deleteCookie('home_dash_calendar_name', $SVC_HOME_DASHBOARD_PATH);
			deleteCookie('home_dash_cid', $SVC_HOME_DASHBOARD_PATH);
			deleteCookie('home_dash_traffic_city', $SVC_HOME_DASHBOARD_PATH);
			deleteCookie('home_dash_traffic_zip', $SVC_HOME_DASHBOARD_PATH);
			deleteCookie('home_dash_weather_city', $SVC_HOME_DASHBOARD_PATH);

		}
	}

?>