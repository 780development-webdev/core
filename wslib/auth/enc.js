/**
	780Development
	Giakhanh Hoang
	
	/wslib/auth/enc.js
    Client encryption script
    
    Requires:
    - /wslib/auth/sodium.js
      For the main encryption library and functions
	
 */

/**
 * Retreives a public key from the server, either a new one or one already generated for
 * the user.
 */
function requestPublicServerKey(pkg) {
	var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            var responseText = this.responseText;
            console.log(responseText);
            if(responseText.includes('//OP_SUCCESS')) {
				onPublicKeyRecieved(pkg, responseText);
            } else {
				onPublicKeyRetrivalFail(pkg, responseText);
            }
        }
    }
    xmlhttp.open('POST', '/wslib/auth/auth.php', true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send('serreqt=publicKey');
}

/**
 * Saves the retreived public key from the server on the client
 * @param String response 
 */
function onPublicKeyRecieved(pkg, response) {
    let enMsg = sodium.crypto_box_seal(
        sodium.from_string(parseMessageFromRequest(pkg)), 
        parsePublicHexKey(response)
    );

	if (pkg.includes(SRV_AUTH_TOKEN)) {
        authenticateTokenSecure(enMsg);
    } else if (pkg.includes(SRV_AUTH_PASS)) {
        
    }
}

function onPublicKeyRetrivalFail(pkg, response) {
    console.error('Failed to get public key for service request');
}

function parseMessageFromRequest(pkg) {
    return pkg.split("//")[1];
}

function parsePublicHexKey(message) {
    var keyPart = message.split("//")[1];
    return sodium.from_hex(keyPart.split("|")[1]);
}

///////////////////////////////////////////////////////////////////////////////
/**
 * Package System details:
 * 
 */

// Eventually should be moved to util.js
let HEAD_MESSAGE = "MESSAGE";

/**
 * 
 * @param String pkg 
 * @param String header 
 * @param String message 
 */
function packMessage(pkg, header, message) {
    if(!message.include("|") || !message.include("//") ||
       !header.include("|") || !header.include("//")) {
        if(message == "") {
            return pkg + "//" + header;
        }
        return pkg + "//" + header + "|" + message;
    }
    console.error("Packaging failed. Illegal characters found in header or message")
}

/**
 * Unpacks a message, given a header search term, from a package string.
 * 
 * *Temporary notes*
 * 
 * Process example:
 *   Given:
 *   - Package: "//AUTH_PASS//MESSAGE|123456"
 *   - Header: "MESSAGE"
 *   Process:
 *   1. Find indexOf "//MESSAGE"
 *   2. Find indexOf next "//" after header location if applicable
 *   3. Return "123456" from "//MESSAGE|123456"
 * 
 * @param String package 
 * @param String header 
 * @return String with message
 */
function unPackMessage(pkg, header) {
    var headIndex = pkg.indexOf("//" + header);
    var endIndex = pkg.indexOf("//", headIndex + 2);
    if(endIndex == -1) {
        endIndex = pkg.length();
    }
    return pkg.subString(headIndex, endIndex).split("|")[1];
}