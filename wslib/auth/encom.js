/**
	780Development
	Giakhanh Hoang
	
	/wslib/auth/encom.js
	Client encryption communications script
 */

 // Client
 function createCommSession() {
     // Create key-pair here
 }

 function send(message, url, urlParams) {
    // Encrypt message 
    // Send encrypted message
 }

 // Server request
 function requestPublicServerKey() {
	var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            var responseText = this.responseText;
            console.log(responseText);
            if(responseText.includes('//OP_SUCCESS')) {
				onPublicKeyRecieved(responseText);
            } else {
				onPublicKeyRetrivalFail(responseText);
            }
        }
    }
    xmlhttp.open('POST', '/wslib/auth/authTest.php', true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send('serreqt=publicKey');
}

function onPublicKeyRecieved(response) {
	var key = response.split("//")[0];
	document.cookie = "svpukey=" + key;
}