  <?php
	/**
		780Development
		Giakhanh Hoang
		
		/wslib/auth/auth.php
		
		This is the main authentication file that should be used to submit authentication requests
		  to.
	 */
	 
	# Library of crucial functions, specifically relating to authentication
    include "authlib.php";

	/**
  		Main script program.
	 */
	main_script();
    function main_script() {
		# Service request processing
		$SERREQT = $_POST['serreqt'];

		# Handle service request, echo the final status state
		if(determineRequest($SERREQT, $SECURE)) {
			echo "//OP_SUCCESS";
		} else {
			echo "//OP_FAILURE";
		}
	}
	
	/**
		Discerns the request that needs to be handled and returns the success status of the
		  operation.
	 */
	function determineRequest($requestType, $isSecure) {
	    if ($requestType == 'verifyLogin') {
            echo "//ERROR_VERIFY_LOGIN_DEPRECATED_UNSECURE";
			return false;
	    } elseif ($requestType == 'createAccount') {
			return createAccount();
	    } elseif ($requestType == 'verifyToken') {
			return verifyToken();
	    } elseif ($requestType == 'passwordChange') {
			return false;
		} elseif ($requestType == 'resetPassword') {
			return resetPassword();
		} elseif ($requestType == 'emailChange') {
			return false;
		} elseif ($requestType == 'sessionClear') {
			return false;
		} elseif ($requestType == 'publicKey') {
			$result = getPublicKey();
			if($result) 
				echo $result;
			return $result;
		} elseif($requestType == 'validateLogin') {
            return validateLogin();  
        } elseif ($requestType == 'decryptMsg') {
			return decryptMessage($_POST["enmsg"]);
		} elseif ($requestType == 'logout') {
			return logout();
		} else {
            return false;
        }
	}
?>