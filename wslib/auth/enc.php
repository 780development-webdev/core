<?php
	/**
		780Development
		Giakhanh Hoang
		
		/wslib/auth/enc.php
		
		This file contains functions relating to encryption only.
		This includes:
		- Public-Private Key Encryption
		- Password Hashing
		
	 */
	 
	// Important credentials file for accessing the MySQL Database
	// This is not accessible to anyone including developers expect the owner
    include "/var/wkeys/skey.php";
	
	define("SESSION_PRIVATE_KEY", "pvKey");
	define("SESSION_PUBLIC_KEY", "puKey");

	/**
		Hashes the given password
	 */
	if(!function_exists("hash_password")) {
		function hash_password($password) {
			return sodium_crypto_pwhash_str(
				$password,
				SODIUM_CRYPTO_PWHASH_OPSLIMIT_INTERACTIVE,
				SODIUM_CRYPTO_PWHASH_MEMLIMIT_INTERACTIVE
			);
		}
	}
	
	/**
		Creates a public-private keypair upon request from a client that will last until
		  the keypair is used to decrypt a message.

		Returns the public key in hexadecimal format.
	 */
	if(!function_exists("getPublicKey")) {
		function getPublicKey() {
			// Create encryption session
			session_start();

			// Create keypairs if session cookies for keypairs are not set
			// Otherwise, its likely the current keypair has not been used for decryption yet
			if(!isset($_SESSION[SESSION_PRIVATE_KEY]) || !isset($_SESSION[SESSION_PUBLIC_KEY])) {
				$server_box_kp = sodium_crypto_box_keypair();
				$server_sign_kp = sodium_crypto_sign_keypair();
	
				$_SESSION[SESSION_PRIVATE_KEY] = sodium_crypto_box_secretkey($server_box_kp);
				$_SESSION[SESSION_PUBLIC_KEY] = sodium_crypto_box_publickey($server_box_kp);	
			}
			
			// Return public key in hexadecimal format
			return "//PU_KEY|".sodium_bin2hex($_SESSION[SESSION_PUBLIC_KEY]);
		}
	}

	/**
		Decrypts a message received from the client using the corresponding session public key.
		Destroys the encryption session when the operation finishes regardless of outcome.
	 */
	if(!function_exists("decryptMessage")) {
		function decryptMessage($chmsg /*, $nonce*/) {
			$msgDisplay = $chmsg;
			//echo "Encrypted Message Received: ".$msgDisplay."\n";
			$msgDisplay = sodium_hex2bin($chmsg);
			//echo "Encrypted Message Convered: ".$msgDisplay."\n";

			// Remember to start session in order to access existing session variables
			session_start();

			// Recreate keypairs from session cookies
			$kp = sodium_crypto_box_keypair_from_secretkey_and_publickey(
				$_SESSION[SESSION_PRIVATE_KEY],
				$_SESSION[SESSION_PUBLIC_KEY]
			);

			// Original design used with the nonce.
			// The nonce would provide extra validation of the sender.
			/*
			$msg = sodium_crypto_box_open(
				$chmsg,
				$nonce,
				$kp
			);
			*/

			// Decrypt message
			$msg = sodium_crypto_box_seal_open(
				$msgDisplay, 
				$kp
			);

			if($msg === false) {
				echo "Message decryption failed\n";
				return false;
			} else {
				echo "Message decryption successful\n";
				return $msg;
			}

			// Decryption is finished, destroy encryption session
			session_destroy();
		}
	}
	
	// Not sure if this will be needed yet.
	if(!function_exists("encryptMessage")) {
		function encryptMessage($msg, $spvKey, $rpuKey) {
			$kp = sodium_crypto_box_keypair_from_secretkey_and_publickey(
				$spvKey,
				$rpuKey
			);
			$nonce = random_bytes(SODIUM_CRYPTO_BOX_NONCEBYTES);
			$chmsg = sodium_crypto_box(
				$msg,
				$nonce,
				$kp
			);
			return $chmsg;
		}
	}
	
?>