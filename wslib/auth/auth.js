/**
	780Development
	Giakhanh Hoang
	
	/wslib/auth/auth.js
    Client authentication script
    
    Requires:
    - /wslib/util/util.js
      For utilities

    Modular authentication script that can be used by any 780Webservices to quickly
    authenticate users. 
    
    Most services will only need authenticateToken() or authenticateTokenSecure().
    All other functions here are mainly for use by the core service.

    *** Services are responsible for implementing the following functions for 
        authenticateToken() or authenticateTokenSecure():
        - onAuthPass(param)
        - onAuthFail(param)

 */

// Landing script for authentication operations
let SERVER_AUTH_PAGE = "wslib/auth/auth.php";

let SRV_AUTH_TOKEN = "AUTH_TOKEN";
let SRV_AUTH_PASS = "AUTH_PASS";
let SRV_AUTH_CRE_ACC = "AUTH_CRE_ACC";

let HEAD_USERNAME = "UNAME";
let HEAD_PASSWD = "PASS";555
let HEAD_STAYLOG = "STAYLOG";
let HEAD_TOKEN = "TOKEN";
let HEAD_PU_KEY = "PU_KEY";
let HEAD_SUCCESS_FLAG = "SUCCESS_FLAG";

let RES_OP_SUCCESS = "OP_SUCCESS";
let RES_AUTH_PASS = "AUTH_SUCCESS";

///////////////////////////////////////////////////////////////////////////////
// Stage 1: Preparation
//          These are the ONLY functions other scripts may call

function validateToken() {
    let token = getCookie('web_userToken');
    if(token != "") {
        var pkg = packMessage("", SRV_AUTH_TOKEN, "");
        pkg = packMessage(pkg, HEAD_TOKEN, token);
        requestPublicServerKey(pkg);
    } else {
        onAuthFail("Token invalid");
    }
}

function validateLogin(username, passwd, staylog) {
    var pkg = packMessage("", SRV_AUTH_PASS, "");
    pkg = packMessage(pkg, HEAD_USERNAME, username);
    pkg = packMessage(pkg, HEAD_PASSWD, passwd);
    pkg = packMessage(pkg, HEAD_STAYLOG, staylog);
    requestPublicServerKey(pkg);
}

///////////////////////////////////////////////////////////////////////////////
// Stage 2: Encryption

/**
 * Retreives a public key from the server, either a new one or one already generated for
 * the user.
 */
function requestPublicServerKey(pkg) {
	var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            var responseText = this.responseText;
            console.log(responseText);
            if(responseText.includes(RES_OP_SUCCESS)) {
				onPublicKeyRecieved(pkg, responseText);
            } else {
				onPublicKeyRetrivalFail(pkg, responseText);
            }
        }
    }
    xmlhttp.open('POST', SERVER_AUTH_PAGE, true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send('serreqt=publicKey');
}

/**
 * Saves the retreived public key from the server on the client
 * 
 * @param String pkg
 * @param String response 
 */
function onPublicKeyRecieved(pkg, response) {
    console.log("Got the following response for public key:");
    console.log(response);
    // Race condition issues, particularly with libsodium being ready to encode when needed
    let pu_key = sodium.from_hex(unPackMessage(response, "PU_KEY"));
	if (pkg.includes(SRV_AUTH_TOKEN)) {
        let enmsg = sodium.from_string(unPackMessage(pkg, HEAD_TOKEN));
        let enTok = sodium.crypto_box_seal(
            enmsg, 
            pu_key
        );
        var nPkg = packMessage("", HEAD_SUCCESS_FLAG, RES_AUTH_PASS);
        performAuthenticationOperation(
            nPkg,
            "serreqt=verifyToken&enTok=" + sodium.to_hex(enTok) 
        );
    } else if (pkg.includes(SRV_AUTH_PASS)) {
        let enmsg = sodium.from_string(unPackMessage(pkg, HEAD_PASSWD));
        let enPass = sodium.crypto_box_seal(enmsg, pu_key);
        var nPkg = packMessage("", HEAD_SUCCESS_FLAG, RES_AUTH_PASS);
        performAuthenticationOperation(
            nPkg,
            "serreqt=validateLogin&uname=" + unPackMessage(pkg, HEAD_USERNAME) + "&pw=" + sodium.to_hex(enPass) + "&staylog=" + unPackMessage(pkg, HEAD_STAYLOG)
        );
    }
}

function onPublicKeyRetrivalFail(pkg, response) {
    console.error('Failed to get public key for service request');
}

///////////////////////////////////////////////////////////////////////////////
// Stage 3: Send

function performAuthenticationOperation(pkg, params) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            var responseText = this.responseText;
            console.log(responseText);
            //var successFlag = unPackMessage(pkg, HEAD_SUCCESS_FLAG);
            if(responseText.includes(RES_OP_SUCCESS)) {
				onAuthOpPass(pkg, responseText);
            } else {
				onAuthOpFail(pkg, responseText);
            }
        }
    }
    xmlhttp.open('POST', SERVER_AUTH_PAGE, true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send(params);
}

///////////////////////////////////////////////////////////////////////////////
// Legacy Operations

/*function authenticateToken(pkg) {
	var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            var responseText = this.responseText;
            console.log(responseText);
            if(responseText.includes(RES_AUTH_PASS)) {
				onAuthPass(responseText);
            } else {
				onAuthFail(responseText);
            }
        }
    }
    xmlhttp.open('POST', SERVER_AUTH_PAGE, true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send('serreqt=verifyToken&secure=true&message=' + message);
}*/

function authenticateLogin(package) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            var responseText = this.responseText;
            console.log(responseText);
            if(responseText.includes('//LOGIN_SUCCESS')) {
				onLoginPass(responseText);
            } else {
				onLoginFail(responseText);
            }
        }
    }
    xmlhttp.open('POST', SERVER_AUTH_PAGE, true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send('serreqt=verifyLogin&uname=' + username + '&pw=' + pass + '&staylog=' + staylog);
}

function authenticateToken() {
	var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            var responseText = this.responseText;
            console.log(responseText);
            if(responseText.includes('//AUTH_SUCCESS')) {
				onAuthPass(responseText);
            } else {
				onAuthFail(responseText);
            }
        }
    }
    xmlhttp.open('POST', SERVER_AUTH_PAGE, true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send('serreqt=verifyToken');
}

function authenticateLogin(username, pass, staylog) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            var responseText = this.responseText;
            console.log(responseText);
            if(responseText.includes('//LOGIN_SUCCESS')) {
				onLoginPass(responseText);
            } else {
				onLoginFail(responseText);
            }
        }
    }
    xmlhttp.open('POST', SERVER_AUTH_PAGE, true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send('serreqt=verifyLogin&uname=' + username + '&pw=' + pass + '&staylog=' + staylog);
}

function createAccount(email, user, pass, pkey) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            var responseText = this.responseText;
            console.log(responseText);
            if(responseText.includes('//AUTH_ACCOUNT_CREATED')) {
				onCreateAccPass(responseText);
            } else {
				onCreateAccFail(responseText);
            }
        }
    }
    xmlhttp.open('POST', SERVER_AUTH_PAGE, true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send('serreqt=createAccount&email=' + email + '&uname=' + user + '&pw=' + pass + '&pkey=' + pkey);
}

function resetPassword(user, pass, ckey) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            var responseText = this.responseText;
            console.log(responseText);
            if(responseText.includes('//AUTH_ACCOUNT_CREATED')) {
				onCreateAccPass(responseText);
            } else {
				onCreateAccFail(responseText);
            }
        }
    }
    xmlhttp.open('POST', SERVER_AUTH_PAGE, true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send('serreqt=resetPassword&uname=' + user + '&pw=' + pass + '&ckey=' + ckey);
}

function logout() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            var responseText = this.responseText;
            document.location = '/';
        }
    }
    xmlhttp.open('POST', SERVER_AUTH_PAGE, true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send('serreqt=logout');
}

///////////////////////////////////////////////////////////////////////////////
// Auth-specific utility functions
function assemblePOSTParams(pkg) {

}

function parseAuthError(response) {
    var code = '';
    if(response.includes('//ERROR')) {
        code = response.split('//')[1];
    }
    switch(code) {
        case 'ERROR-100': return 'Invalid credentials';
        case 'ERROR-101': return 'Invalid token';
        case 'ERROR-103': return 'Unknown Error - 103';
        case 'ERROR-104': return 'Invalid credentials';
        case 'ERROR-112': return 'Invalid key';
        case 'ERROR-119': return 'Failed to create account';
        case 'ERROR-199': return 'Invalid credentials';
        case 'ERROR-300': return 'Unknown Error - 300';
        case 'ERROR-999': return 'Unknown Error - 999';
        default: return response;
    }
}