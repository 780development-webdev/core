const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = 7900

app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended: true
    })
)

app.get('/', (request, response) => {
    response.json({
        info: "780Webservices authentication backend.",
        code: 0,
        message: "Please send a proper request to the authentication system"
    })
})

app.listen(port, () => {
    console.log('Server running on port ' + port + '.')
})