<?php
	/**
		780Development
		Giakhanh Hoang
		
		/wslib/util/util.php
		
		This file contains utility functions that simply operations.
		This includes:
		- Simplified MySQL operations
		- Random string generation
		
	 */
	 
	// Important credentials file for accessing the MySQL Database
	// This is not accessible to anyone including developers expect the owner
    include "/var/wkeys/skey.php";
	
	/**
		Creates a prepared MySQL query with only one parameter and executes it
		
		Returns the first row only
	 */
	if(!function_exists("mysqli_query_execute_and_result")) {
		function mysqli_query_execute_and_result($con, $stmt, $stmt_params_types, $stmt_param) {
			$cmd = mysqli_prepare($con, $stmt);
			mysqli_stmt_bind_param($cmd, $stmt_params_types, $stmt_param);
			mysqli_stmt_execute($cmd);
			mysqli_stmt_bind_result($cmd, $valueRes);
			mysqli_stmt_fetch($cmd);
			mysqli_stmt_close($cmd);
			return $valueRes;
		}
	}
	
	/**
		Creates a prepared MySQL query with only one parameter and executes it.
		
		Returns whether execution was successful
	 */
	if(!function_exists("mysqli_query_and_execute")) {
		function mysqli_query_and_execute($con, $stmt, $stmt_params_types, $stmt_param) {
			$cmd = mysqli_prepare($con, $stmt);
			mysqli_stmt_bind_param($cmd, $stmt_params_types, $stmt_param);
			$result = mysqli_stmt_execute($cmd);
			mysqli_stmt_close($cmd);
			return $result;
		}
	}
	
	/**
		Generates a series of random characters in a string.
		Used for password/token generation.
		
		Default length: 32
	 */
    if(!function_exists('generateRandomString')) {
        function generateRandomString($length = 32) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }
    }

    /**
        Delete the cookie if it exists
      */
    if(!function_exists('deleteCookie')) {
        function deleteCookie($cookieName, $cookiePath) {
			setcookie($cookieName, '', 1, $cookiePath);
        }
    }
	
?>