/**
	780Development
	Giakhanh Hoang
	
	/wslib/util/util.js
	
	General utility functions, mostly relating to dates so far with recent projects.
	**This may change in the future.
 */
 
/**
	Returns the value of the given cookie name.
 */
function getCookie(name) {
	// Is the space after the semicolon necessary?
    var cookieArr = document.cookie.split('; ');
    for(var i = 0; i < cookieArr.length; i++) {
        var cookieParts = cookieArr[i].split('=');
            if(cookieParts[0] == name) {
                if(cookieParts[1].charAt(cookieParts[1].length-1) == ';') {
                    return cookieParts[1].substr(0, cookieParts[1].length-1);
                } else {
                    return cookieParts[1].substr(0, cookieParts[1].length);
                }
            }
        }
    return '';
}

/**
	Returns the corresponding month string value, beginning with 0
 */
function formatMonth(number) {
    switch(number) {
        case 0:
            return 'January';
            break;
        case 1:
            return 'February';
            break;
        case 2:
            return 'March';
            break;
        case 3:
            return 'April';
            break;
        case 4:
            return 'May';
            break;
        case 5:
            return 'June';
            break;
        case 6:
            return 'July';
            break;
        case 7:
            return 'August';
            break;
        case 8:
            return 'September';
            break;
        case 9:
            return 'October';
            break;
        case 10:
            return 'November';
            break;
        case 11:
            return 'December';
            break;
        default:
            return 'Nonexistant';
            break;
    }
}

/**
	Takes a single digit number and returns 0 to the beginning as a string.
	Ex. '1' >> '01'
 */
function formatNumber00(number) {
    if(number >= 0 && number < 10) {
        return '0' + number;
    }
    return number;
}

/**
    Outputs a formatted time in HH:MM AM/PM if zeroLeadingHour is true.
    Otherwise the formatted time will be H:MM AM/PM.
 */
function formatTime(hour, minutes, zeroLeadingHour) {
    var hourStr = (hour + 1);
    if(zeroLeadingHour && (hour < 10)) {
        hourStr = '0' + (hour + 1);
    }
    var minuteStr = minutes;
    if(minuteStr < 10) {
        minuteStr = '0' + minutes;
    }
    var ampm = 'AM';
    if(hour > 12) {
        ampm = 'PM';
    }
    return hourStr + ':' + minuteStr + ' ' + ampm;
}

/**
    Outputs a formatted date range string. Requires two date objects.
    There is no time precision (aka time displayed) if the range is larger than a month.
 */
function formatDateRange(dateStart, dateEnd) {
    // Exact same date and time
    if(dateStart.getYear() == dateEnd.getYear() && dateStart.getMonth() == dateEnd.getMonth() && dateStart.getDate() == dateEnd.getDate() 
            && dateStart.getHours() == dateEnd.getHours() && dateStart.getMinutes() == dateEnd.getMinutes()) {
        return formatMonth(dateStart.getMonth()) + ' ' + dateStart.getDate() + ', ' + (dateStart.getYear() + 1900);
    } 
    // Same date, different time
    else if (dateStart.getYear() == dateEnd.getYear() && dateStart.getMonth() == dateEnd.getMonth() && dateStart.getDate() == dateEnd.getDate()) {
        return formatMonth(dateStart.getMonth()) + ' ' + dateStart.getDate() + ', ' + (dateStart.getYear() + 1900) + ' ' + 
            formatTime(dateStart.getHours(), dateStart.getMinutes(), false) + ' to ' +
            formatTime(dateEnd.getHours(), dateEnd.getMinutes(), false);
    }
    // Same month and year, different day
    else if(dateStart.getDate() != dateEnd.getDate() && dateStart.getMonth() == dateEnd.getMonth() && dateStart.getYear() == dateEnd.getYear()) {
        // but same time
        if(dateStart.getHours() == dateEnd.getHours() && dateStart.getMinutes() == dateEnd.getMinutes()) {
            return formatMonth(dateStart.getMonth()) + ' ' + dateStart.getDate() + ' to ' + dateEnd.getDate() + ', ' + (dateStart.getYear() + 1900);
        } else {
            return formatMonth(dateStart.getMonth()) + ' ' + dateStart.getDate() + ' ' + formatTime(dateStart.getHours(), dateStart.getMinutes(), false) + ' to ' 
                + dateEnd.getDate() + ' ' + formatTime(dateEnd.getHours(), dateEnd.getMinutes(), false) + ', ' + (dateEnd.getYear() + 1900);
        }
    }
    // Same year, different month and day, not going to be specific on time because of this large range
    else if(dateStart.getMonth() != dateEnd.getMonth() && dateStart.getYear() == dateEnd.getYear()) {
        return formatMonth(dateStart.getMonth()) + ' ' + dateStart.getDate() + ' to ' +
            formatMonth(dateEnd.getMonth()) + ' ' + dateEnd.getDate() + ' ' + (dateEnd.getYear() + 1900);
    }
    // Totally different in all aspects, all else, not going to be specific on time because of this large range
    else {
        return formatMonth(dateStart.getMonth()) + ' ' + dateStart.getDate() + ', ' + (dateStart.getYear() + 1900) 
            + ' to ' +
            formatMonth(dateEnd.getMonth()) + ' ' + dateEnd.getDate() + ', ' + (dateEnd.getYear() + 1900);
    }
}

/**
    Converts an SQL Timestamp to a Javascript date object
  */
function convertSQLTimeToJSTime(date) {
    var dateParts = date.split(/[- :]/);
    var jsDate = new Date(dateParts[0],         // Year
                            dateParts[1] - 1,   // Month
                            dateParts[2],       // Day
                            dateParts[3],       // Hour
                            dateParts[4],       // Minute
                            dateParts[5]);      // Second
    return jsDate;
}

/**
    Formats a date object to be used in an input element of type datetime.
  */
function formatToDateTimeInput(date) {
    return (date.getYear() + 1900) + '-' + formatNumber00(date.getMonth() + 1) + '-' + formatNumber00(date.getDate()) + 
         'T' + formatNumber00(date.getHours()) + ':' + formatNumber00(date.getMinutes()) + ':00';
}

/**
	Takes in a string input and checks for prohibited characters
    
    TODO: Should implement a function that corrects/replaces invalid chars with equivalents
 */
function sanitizeSQLInput(input) {
    for(var i = 0; i < input.length; i++) {
        // Any character that isn't 0-9, A to Z, a to z, or '_' will fail the test
        if(input.charCodeAt(i) < 48
           || (input.charCodeAt(i) > 57 && input.charCodeAt(i) < 65) 
           || (input.charCodeAt(i) > 90 && input.charCodeAt(i) < 95)
           || (input.charCodeAt(i) > 95 && input.charCodeAt(i) < 97) 
           || (input.charCodeAt(i) > 122)) {
            return false;
        }
    }
    return true;
}

/**
    Validates email address, checks for an '@' symbol and period '.'
 */
function validateEmailFormat(input) {
    return input.includes('@') && input.includes('.');
}

///////////////////////////////////////////////////////////////////////////////
/**
 * Package System details:
 * 
 */

// Eventually should be moved to util.js
let HEAD_MESSAGE = "MESSAGE";

/**
 * 
 * @param String pkg 
 * @param String header 
 * @param String message 
 */
function packMessage(pkg, header, message) {
    if(!message.includes("|") && !message.includes("//") &&
       !header.includes("|") && !header.includes("//")) {
        if(message == "") {
            return pkg + "//" + header;
        }
        return pkg + "//" + header + "|" + message;
    } else console.error("Packaging failed. Illegal characters found in header or message")
}

/**
 * Unpacks a message, given a header search term, from a package string.
 * 
 * *Temporary notes*
 * 
 * Process example:
 *   Given:
 *   - Package: "//AUTH_PASS//MESSAGE|123456"
 *   - Header: "MESSAGE"
 *   Process:
 *   1. Find indexOf "//MESSAGE"
 *   2. Find indexOf next "//" after header location if applicable
 *   3. Return "123456" from "//MESSAGE|123456"
 * 
 * @param String package 
 * @param String header 
 * @return String with message
 */
function unPackMessage(pkg, header) {
    var headIndex = pkg.indexOf("//" + header);
    if (headIndex == -1) {
        console.error("Could not find specified header.");
        return;
    }
    var endIndex = pkg.indexOf("//", headIndex + 2);
    if(endIndex == -1) {
        endIndex = pkg.length;
    }
    var length = endIndex - headIndex;
    console.log("Unpacking header " + header + " with headInx " + headIndex + ", endInx " + endIndex + ", length " + length + " for result: " + pkg.substr(headIndex, length).split("|")[1]);
    return pkg.substr(headIndex, length).split("|")[1];
}